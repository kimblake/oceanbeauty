<?php

/**
 * Search multidimensional arrays. Inelegant but works.
 */
function search_array($needle, $haystack) {
	
	foreach($haystack as $key => $element) {
		
		// Cursory check if $element is in non multidimensional array.
		if( count($element) == count($element, COUNT_RECURSIVE) ) {
			if( in_array($needle, $element) ) {
				return $key;
			}
		} else { // If $element is multidimensional (most of the time)
			foreach($element as $subelement) {
				if(!is_array($subelement)) {
					if($needle == $subelement) {
						return $key;
					}
				} else {
					if( in_array($needle, $subelement) ) {
						return $key;	
					}
				}
			}
		}
		
	}
	
	return "default";

}

/**
 * @return array Closest distribution location by 
 */
function distributor_locate($zipcode) {

	// All OceanBeauty distribution locations and their zipcodes. It would be simpler to derive city from zip code but the project requirements are very specific.
	$locations = array(
		'helena' => range(59000, 59937),
		'dallas' => array( range(73000,73344), range(75000,75399), range(76000,76249), range(77000,77399), range(78200,78299), range(78600,78799) ),
		'portland' => array( range(97000,97015), 97017, range(97019,97047), range(97049,97050), 97055, range(97057,97101), range(97104,97106), 97111, range(97113,97117), range(97119,97120), range(97122,97129), range(97132,97133), 97140, 97148, range(97201,97340), range(97342,97363), range(97370,97375), range(97377,97379), range(97381,97387), range(97389,97390), range(97392,97393), range(97395,97497), range(97701,97707), 97730, 97739, range(97753,97760), range(98604,98610), 98642, range(98660,98665), 98674, range(98682,98686), range(98682,98686) ),
		'astoria' => array(97016, 97018, 97048, range(97051,97054), 97056, range(97102,97103), range(97107,97110), 97112, 97118, 97121, range(97130,97131), range(97134,97139), range(97141,97147), 97149, 97341, range(97364,97369), 97376, 97380, 97388, 97391, 97394, range(97498,97499), 98527, 98577, 98586, range(98611,98641), range(98643,98647) ),
		'seattle' => array( range(98000,98599), 98654, range(99000,99039), range(99200,99224) ),
		'boise' => array( range(83200, 83716), 82443 ),
		'salt_lake' => array( range(83001,83002), range(84001,84790) )
	);

    if(empty($zipcode)) {
	      // Grab zip code from visitor
	      $ip_address = $_SERVER['REMOTE_ADDR'];
	      
	      $query = @unserialize( file_get_contents( 'http://ip-api.com/php/' . $ip_address ) );
        
	      if($query && $query['status'] == 'success' && $query['countryCode'] == 'US') {
            
		        // Grab location from zip
		        $location = search_array($query['zip'], $locations);
            
	      } else {
		        // If zip code was not recieved or not in U.S.

            $location = "default";
        }
    } else {
		    $location = search_array($zipcode, $locations);
    }
	
	return $location;

}

function oceanbeauty_create_post_type() {
  register_post_type( 'ob_species',
    array(
      'labels' => array(
        'name' => __( 'Species' ),
        'singular_name' => __( 'Species' )
      ),
      'public' => true,
      'supports' => array(
	      'title', 'editor', 'thumbnail'
      ),
      'taxonomies' => array('category'),
      'has_archive' => false,
      'map_meta_cap' => true
    )
  );
}

add_action( 'init', 'oceanbeauty_create_post_type' );

function oceanbeauty_species_shortcode($atts) {
	global $post;
	$args = array( 'posts_per_page' => 5, 'category_name' => $atts['category'], 'post_type' => 'ob_species' );
	$myposts = get_posts( $args );
	$species_markup = '';
	foreach ( $myposts as $post ) :
		$species_markup .= '<li>' . 
    get_the_post_thumbnail($post->ID) .'<div class="species-block"><h4>' . 
    get_the_title() . '</h4><p>' . 
    $post->post_content . '</p></div><div class="species-footer';
    $cat = get_field('species_category');
    if($cat == 'Wild Alaska') $species_markup .= ' wild-alaska ';
    else if($cat == 'Wild Caught') $species_markup .= ' wild-caught ';
    else if($cat == 'Farm Raised') $species_markup .= ' farm-raised ';
    $species_markup .= '"><div class="category">' . get_field_object('species_category')['value'] . '</div>';
    if($cat == 'Wild Alaska') {
        $species_markup .= '<img src="http://www.oceanbeauty.dev/wp-content/uploads/2014/11/alaska-seafood-watermark.png">';
    }
    $species_markup .= '</div></li>';
	endforeach; 
	wp_reset_postdata();
	return '<ul id="species-list" class="clearfix">' . $species_markup . '</ul>';
}

add_shortcode( 'ob_species', 'oceanbeauty_species_shortcode' );

function oceanbeauty_scripts() {
	wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri() . '/js/script.js', array(), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'oceanbeauty_scripts' );


function oceanbeauty_home_footer() {
        // Home Footer       
        
         register_sidebar( array(
            'id'            => 'home-footer-column-1',
            'name'          => __( 'Home Footer Column 1' ),
            'description'   => __( 'Widgets in this area will be shown on the home page' ),
            'before_widget' => '<div class ="home-footer-menu">',
            'after_widget' => '</div>',
            'before_title' => '<div class="widget-heading">',
            'after_title' => '</div>',
        ));
        
        register_sidebar( array(
            'id'            => 'home-footer-column-2',
            'name'          => __( 'Home Footer Column 2' ),
            'description'   => __( 'Widgets in this area will be shown on the home page' ),
            'before_widget' => '<div class ="home-footer-menu">',
            'after_widget' => '</div>',
            'before_title' => '<div class="widget-heading">',
            'after_title' => '</div>',
        )); 
        
        register_sidebar( array(
            'id'            => 'home-footer-column-3',
            'name'          => __( 'Home Footer Column 3' ),
            'description'   => __( 'Widgets in this area will be shown on the home page' ),
            'before_widget' => '<div class ="home-footer-menu">',
            'after_widget' => '</div>',
            'before_title' => '<div class="widget-heading">',
            'after_title' => '</div>',
        )); 
        
        register_sidebar( array(
            'id'            => 'home-footer-column-4',
            'name'          => __( 'Home Footer Column 4' ),
            'description'   => __( 'Widgets in this area will be shown on the home page' ),
            'before_widget' => '<div class ="home-footer-menu">',
            'after_widget' => '</div>',
            'before_title' => '<div class="widget-heading">',
            'after_title' => '</div>',
        ));      
}
    add_action( 'widgets_init', 'oceanbeauty_home_footer' );

?>
