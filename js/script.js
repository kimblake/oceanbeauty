jQuery(document).ready(function($) {
	
	var header = $('#header');
	var nav = $('#main-nav');
	
	var navMobile = $('.visible-sm');
	
	header.data('size','big');
	var headerHeight = header.height();

	$(window).scroll(function(){

	if($(document).scrollTop() > headerHeight) {
		if(header.data('size') == 'big')
		{
		    header.data('size','small');
			
			nav.addClass('fixed');
			
			navMobile.addClass('fixed');
			
		}
	}
	
	else {

	    if(header.data('size') == 'small') {
			header.data('size','big');
			nav.removeClass('fixed');		
			navMobile.removeClass('fixed');
		}

	}
	});

});