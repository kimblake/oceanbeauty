<?php
if(isset($_GET['zip'])) {
    $location = distributor_locate($_GET['zip']);
} else {
    $location = distributor_locate(null);
}

// Simply header to correct pages.

$found = true;
switch ($location) {
	case "helena":
		header('Location: ' . get_bloginfo( 'url' ) . '/distribution/helena/');
		break;
	case "dallas":
		header('Location: ' . get_bloginfo( 'url' ) . '/distribution/dallas/');
		break;
	case "portland":
		header('Location: ' . get_bloginfo( 'url' ) . '/distribution/portland/');
		break;
	case "astoria":
		header('Location: ' . get_bloginfo( 'url' ) . '/distribution/astoria/');
		break;
	case "seattle":
		header('Location: ' . get_bloginfo( 'url' ) . '/distribution/seattle/');
		break;
	case "boise":
		header('Location: ' . get_bloginfo( 'url' ) . '/distribution/boise/');
		break;
	case "salt_lake":
		header('Location: ' . get_bloginfo( 'url' ) . '/distribution/salt-lake/');
		break;
	case "default":
		header('Location: ' . get_bloginfo( 'url' ) . '/distribution/fresh-direct/');
		break;
  default:
    $found = false;
    break;
}
if(!$found) {
    die();
}
?>
